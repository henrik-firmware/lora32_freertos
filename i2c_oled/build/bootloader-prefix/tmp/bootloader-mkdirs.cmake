# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/home/henrik/esp/esp-idf/components/bootloader/subproject"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/tmp"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/src/bootloader-stamp"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/src"
  "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/home/henrik/Desktop/lora32_freertos/i2c_oled/build/bootloader-prefix/src/bootloader-stamp${cfgdir}") # cfgdir has leading slash
endif()
