# lora32_freertos



## Board info

- TTGO LoRa32 915Mhz
- Model T3_V1.6.1(20210104)

## Dev framework
- ESP IDF v.5.1.2

## License
For open source projects, say how it is licensed.